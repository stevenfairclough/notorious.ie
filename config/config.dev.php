<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Development config overrides & db credentials
 * 
 * Our database credentials and any environment-specific overrides
 * 
 * @package    Focus Lab Master Config
 * @version    1.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$env_db['hostname'] = 'localhost';
$env_db['username'] = 'root';
$env_db['password'] = 'root';
$env_db['database'] = 'notorious_local';

// Sample global variable for Production only
// Can be used in templates like "{global:google_analytics}"
//$env_global['global:google_analytics'] = 'UA-XXXXXXX-XX';

// EMAIL CONFIG VARS
// THE FOLLOWING WILL WORK FOR GMAIL SPECIFICALLY
        /*
$config['mail_protocol']  = 'smtp';
$config['smtp_server']    = 'tls://smtp.gmail.com:465';
$config['smtp_username']  = 'xxx@xxx.xx';
$config['smtp_password']  = 'xxx';
$config['email_newline']  = "\r\n";
$config['email_crlf']    = "\r\n";
        */

// SETTINGS TO RESET WHATEVER IS IN THE DB - ONLY THE ONES WE NEED TO SET HERE!
//$config['email_debug']      = 'n';
$config['webmaster_email']  = "steve@firstcom.ie";
$config['webmaster_name']   = "PSG Group";
//$config['email_crlf']       = "\r\n"; // default is "\n"
//$config['email_newline']    = "\r\n"; // default is "\n"
$config['mail_protocol']    = 'mail';
$config['smtp_server']      = '';
//$config['smtp_port']        = '587'; // Not sure if this is used or not, seems legit, too
//$config['email_smtp_port']  = '587';
$config['smtp_username']    = '';
$config['smtp_password']    = '';
//$config['smtp_crypto']      = 'tls'; // CodeIgniter setting, doesn't seem to be used
//$config['email_smtp_crypto']= 'tls';


/* End of file config.prod.php */
/* Location: ./config/config.prod.php */