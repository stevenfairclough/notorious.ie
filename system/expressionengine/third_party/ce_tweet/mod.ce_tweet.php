<?php if ( ! defined( 'BASEPATH' ) ) exit('No direct script access allowed');
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
/*
====================================================================================================
 Author: Aaron Waldon
 http://www.causingeffect.com
 license http://www.causingeffect.com/software/expressionengine/ce-tweet/license-agreement
====================================================================================================
 This file must be placed in the /system/expressionengine/third_party/ce_tweet folder in your ExpressionEngine installation.
 package 		CE Tweet
 copyright 		Copyright (c) 2013 Causing Effect
----------------------------------------------------------------------------------------------------
 Purpose: Integrate with Twitter.
====================================================================================================
License:
    CE Tweet is licensed under the Commercial License Agreement found at http://www.causingeffect.com/software/commercial-license
	Here are a few specific points from the license to note:
    * One license grants the right to perform one installation of CE Tweet. Each additional installation of CE Tweet requires an additional purchased license.
    * You may not reproduce, distribute, or transfer CE Tweet, or portions thereof, to any third party.
	* You may not sell, rent, lease, assign, or sublet CE Tweet or portions thereof.
	* You may not grant rights to any other person.
	* You may not use CE Tweet in violation of any United States or international law or regulation.
*/

include( PATH_THIRD . 'ce_tweet/config.php' );

class Ce_tweet {

	//debug mode flag
	private $debug = false;

	//will hold the linking options
	private $linking = array();

	//variable prefix
	private $var_prefix = '';

	/**
	 * Constructor
	 */
	public function __construct()
	{
		//EE super-object
		$this->EE = get_instance();

		//if the template debugger is enabled, and a super admin user is logged in, enable debug mode
		$this->debug = false;
		if ( $this->EE->session->userdata['group_id'] == 1 && $this->EE->config->item('template_debugging') == 'y' )
		{
			$this->debug = true;
		}
	}

	//-------------------------------------------- Tags --------------------------------------------

	//---------------------- Timelines ----------------------
	/**
	 * Returns the most recent statuses, including retweets, by the user and the user's they follow.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
	 * @return string
	 */
	public function home_timeline()
	{
		$params = $this->fetch_twitter_params( array( 'count', 'since_id', 'max_id', 'trim_user', 'include_rts', 'include_entities', 'exclude_replies', 'contributor_details' )  );

		return $this->get_response( __FUNCTION__, '/statuses/home_timeline.json', $params );
	}

	/**
	 * Returns the most recent mentions (status containing @username) for the authenticating user.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/statuses/mentions_timeline
	 * @return string
	 */
	public function mentions()
	{
		$params = $this->fetch_twitter_params( array( 'count', 'since_id', 'max_id', 'trim_user', 'include_rts', 'include_entities', 'contributor_details' ) );

		return $this->get_response( __FUNCTION__, '/statuses/mentions_timeline.json', $params );
	}

	/**
	 * Alias for the mentions method. This is the new name as of API version 1.1.
	 *
	 * @return string
	 */
	public function mentions_timeline()
	{
		return $this->mentions();
	}

	/**
	 * Returns the most recent tweets of the authenticated user that have been retweeted by others.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/statuses/retweets_of_me
	 * @return string
	 */
	public function retweets_of_me()
	{
		$params = $this->fetch_twitter_params( array( 'count', 'since_id', 'max_id', 'trim_user', 'include_entities', 'include_user_entities' ) );

		return $this->get_response( __FUNCTION__, '/statuses/retweets_of_me.json', $params );
	}

	/**
	 * Returns the most recent statuses posted by the specified user.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline
	 * @return string
	 */
	public function user_timeline()
	{
		$params = $this->fetch_twitter_params( array( 'user_id', 'screen_name', 'since_id', 'count', 'max_id', 'trim_user', 'include_rts', 'include_entities', 'exclude_replies', 'contributor_details' ) );

		if ( ! isset( $params['user_id'] ) && ! isset( $params['screen_name'] ) )
		{
			$this->log_debug_messages( 'You must specify a user_id= or screen_name= when using the "user_timeline" tag.' );
			return '';
		}

		return $this->get_response( __FUNCTION__, '/statuses/user_timeline.json', $params );
	}

	//---------------------- Tweets ----------------------
	/**
	 * Returns up to 100 of the first retweets of a given tweet.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/retweets/%3Aid
	 * @return string
	 */
	public function retweets()
	{
		//id
		$id = $this->EE->TMPL->fetch_param( 'id', '' );
		if ( empty( $id ) || ! is_numeric( $id ) )
		{
			$this->log_debug_messages( 'You must specify an id= when using the "retweets" tag.' );
			return '';
		}

		$params = $this->fetch_twitter_params( array( 'count', 'trim_user', 'include_entities' ) );

		//limit count to 100
		if ( $params['count'] > 100 )
		{
			$params['count'] = 100;
		}

		return $this->get_response( __FUNCTION__, '/statuses/retweets/' . $id . '.json', $params );
	}

	/**
	 * Returns a single status, specified by the id parameter below. The status's author will be returned inline.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/show/%3Aid
	 * @return string
	 */
	public function show()
	{
		//id
		$id = $this->EE->TMPL->fetch_param( 'id', '' );
		if ( empty( $id ) )
		{
			$this->log_debug_messages( 'You must specify an id= when using the "show" tag.' );
			return '';
		}

		if ( ! is_numeric( $id ) )
		{
			if ( is_numeric( str_replace( '|', '', $id ) ) ) //the user has multiple ids specified, show them 1 by 1
			{
				//get the var prefix
				$tag_parts = $this->EE->TMPL->tagparts;
				$var_prefix = ( is_array( $tag_parts ) && isset( $tag_parts[2] ) ) ? $tag_parts[2] . ':' : '';

				//get the tag parameters
				$tag_params = $this->EE->TMPL->tagparams;

				//unset the id parameter
				unset( $tag_params['id'] );

				//reconstruct the parameters
				$parameters = '';
				foreach ( $tag_params as $param => $value )
				{
					$parameters .= ( strpos( $value, '"' ) === false) ? "{$param}=\"{$value}\" " : "{$param}='{$value}' ";
				}

				unset( $tag_params );

				$output = '';
				$ids = explode( '|', $id );
				foreach( $ids as $id )
				{
					$output .= '{exp:ce_tweet:show' . $var_prefix . ' id="' . $id . '" ' . $parameters . '}';
					$output .= $this->EE->TMPL->tagdata;
					$output .= '{/exp:ce_tweet:show}';
				}

				$output = $this->parse_switch_variables( $output );

				return $output;
			}
			else
			{
				$this->log_debug_messages( 'You must specify a numeric id= when using the "show" tag.' );
				return '';
			}
		}

		$params = $this->fetch_twitter_params( array( 'trim_user', 'include_entities' ) );

		$include_my_retweet = $this->EE->TMPL->fetch_param( 'include_my_retweet', 'no' );

		if ( $this->ee_string_to_bool( $include_my_retweet ) )
		{
			$params['include_my_retweet'] = true;
		}

		return $this->get_response( __FUNCTION__, '/statuses/show/' . $id . '.json', $params, true );
	}

	/**
	 * Parse switch variables in tag data.
	 * @param $string
	 * @return mixed
	 */
	protected function parse_switch_variables( $string )
	{
		if ( strpos( $string,  LD . 'switch' ) === false )
		{
			return $string;
		}

		//Parse {switch="foo|bar"} variables in the tagdata. This code is similar to the {switch=} variable code in EE.
		if ( preg_match_all( '/' . LD . '(switch\s*=.+?)' . RD . '/i', $string, $matches, PREG_SET_ORDER ) ) //match the variables
		{
			foreach ($matches as $match) //loop through the matches
			{
				$params = $this->EE->functions->assign_parameters($match[1]);

				if ( isset( $params['switch'] ) )
				{
					//explode the parameters. Wrap in * to preserve whitespace. Excape with '\' want to use * at beginning or end of string.
					$switch_options = explode( '|', str_replace('\*', '*', trim($params['switch'], '*') ) );

					$i = 1;
					while ( ($pos = strpos( $string, LD . $match[1] . RD ) ) !== false )
					{
						$string = substr_replace( $string, $switch_options[( $i++ + count($switch_options) - 1) % count($switch_options)], $pos, strlen( LD . $match[1] . RD ) );
					}
				}
			}
		}

		return $string;
	}

	/**
	 * Returns information allowing the creation of an embedded representation of a Tweet on third party sites
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/statuses/oembed
	 * @return string
	 */
	public function oembed()
	{
		$id = $this->EE->TMPL->fetch_param( 'id', '' );
		$url = $this->EE->TMPL->fetch_param( 'url', '' );

		$params = array();

		$which = '';

		if (! empty($id))
		{
			$which = 'id';
			$params['id'] = $id;
		}
		else if (! empty($url))
		{
			$which = 'url';
			$params['url'] = urlencode($url);
		}
		else
		{
			$this->log_debug_messages( 'You must specify an id= and url= when using the "oembed" tag.' );
			return '';
		}

		if ( strpos( $$which, '|' ) !== false ) //the user has multiple tweet ids or urls, show them 1 by 1
		{
			//get the var prefix
			$tag_parts = $this->EE->TMPL->tagparts;
			$var_prefix = ( is_array( $tag_parts ) && isset( $tag_parts[2] ) ) ? $tag_parts[2] . ':' : '';

			//get the tag parameters
			$tag_params = $this->EE->TMPL->tagparams;

			//unset the $which parameter
			unset( $tag_params[$which] );

			//reconstruct the parameters
			$parameters = '';
			foreach ( $tag_params as $param => $value )
			{
				$parameters .= ( strpos( $value, '"' ) === false) ? "{$param}=\"{$value}\" " : "{$param}='{$value}' ";
			}

			unset( $tag_params );

			$output = '';
			$ids = explode( '|', $$which );
			foreach( $ids as $id )
			{
				$output .= '{exp:ce_tweet:oembed'.$var_prefix.' '.$which.'="'.$id.'" '.$parameters.'}';
				$output .= $this->EE->TMPL->tagdata;
				$output .= '{/exp:ce_tweet:oembed}';
			}

			$output = $this->parse_switch_variables( $output );

			return $output;
		}

		$maxwidth = $this->EE->TMPL->fetch_param( 'maxwidth', '' );
		if (! empty($maxwidth) && is_numeric($maxwidth))
		{
			if ( $maxwidth < 250 )
			{
				$maxwidth = 250;
			}
			else if ( $maxwidth > 550 )
			{
				$maxwidth = 550;
			}
			$params['maxwidth'] = $maxwidth;
		}

		$hide_media = $this->EE->TMPL->fetch_param( 'hide_media', 'n' );
		if (! empty($hide_media))
		{
			$params['hide_media'] = $this->ee_string_to_bool($hide_media);
		}

		$hide_thread = $this->EE->TMPL->fetch_param( 'hide_thread', 'n' );
		if (! empty($hide_thread))
		{
			$params['hide_thread'] = $this->ee_string_to_bool($hide_thread);
		}

		$params['omit_script'] = true;

		$align = $this->EE->TMPL->fetch_param( 'align', 'none' );
		if (! in_array($align, array('left','right','center','none')))
		{
			$params['align'] = $align;
		}

		$lang = $this->EE->TMPL->fetch_param( 'lang', '' );
		if (! empty($lang))
		{
			$params['lang'] = $lang;
		}

		return $this->get_response( __FUNCTION__, '/statuses/oembed.json', $params, true, false );
	}

	//---------------------- Search ----------------------
	/**
	 * Returns tweets that match a specified query.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/search/tweets
	 * @return string
	 */
	public function search()
	{
		//get the parameters
		$params = $this->fetch_twitter_params( array( 'since_id', 'include_entities' ) );

		//query
		$q = $this->EE->TMPL->fetch_param( 'q' );
		if ( ! empty( $q ) )
		{
			$params['q'] = $q;
		}
		else
		{
			$this->log_debug_messages( 'You must specify a q= parameter for the "search" tag.' );
			return '';
		}

		//geocode
		$geocode = $this->EE->TMPL->fetch_param( 'geocode' );
		if ( ! empty( $geocode ) )
		{
			$geocode = str_replace( '|', ',', $geocode );
			$params['geocode'] = $geocode;
		}

		//count (rpp)
		$rpp = $this->EE->TMPL->fetch_param( 'count', 20 );
		if ( ! empty( $rpp ) )
		{
			if ( is_numeric( $rpp ) )
			{
				if ( $rpp > 100 )
				{
					$rpp = 100;
				}
				$params['rpp'] = $params['count'] = $rpp;
			}
		}

		//lang
		$lang = $this->EE->TMPL->fetch_param( 'lang' );
		if ( ! empty( $lang ) )
		{
			$params['lang'] = $lang;
		}

		//until
		$until = $this->EE->TMPL->fetch_param( 'until' );
		if ( ! empty( $until ) )
		{
			$params['until'] = $until;
		}

		//result_type
		$result_type = $this->EE->TMPL->fetch_param( 'result_type', 'mixed' );
		$params['result_type'] = ( empty( $result_type ) || ! in_array( $result_type, array( 'mixed', 'recent', 'popular' ) ) ) ? 'recent' : $result_type;

		return $this->get_response( __FUNCTION__, '/search/tweets.json', $params, true );
	}

	//---------------------- Users ----------------------
	public function users_show()
	{
		$params = $this->fetch_twitter_params( array( 'user_id', 'screen_name', 'include_entities' ) );

		if ( ! isset( $params['user_id'] ) && ! isset( $params['screen_name'] ) )
		{
			$this->log_debug_messages( 'You must specify a user_id= or screen_name= when using the "users_show" tag.' );
			return '';
		}

		return $this->get_response( __FUNCTION__, '/users/show.json', $params, true, false );
	}

	/**
	 * Alias for the users_show method.
	 *
	 * @return string
	 */
	public function show_user()
	{
		return $this->users_show();
	}

	//---------------------- Favorites ----------------------
	/**
	 * Returns the most recent favorite statuses for the authenticating user or user specified by the ID parameter in the requested format.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/favorites/list
	 * @return string
	 */
	public function favorites()
	{
		$params = $this->fetch_twitter_params( array( 'count', 'user_id', 'screen_name', 'since_id', 'max_id', 'include_entities' )  );

		return $this->get_response( __FUNCTION__, '/favorites/list.json', $params );
	}

	//---------------------- Lists ----------------------
	/**
	 * Returns the tweet timeline for members of the specified list.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/lists/statuses
	 * @return string
	 */
	public function lists_statuses()
	{
		$params = $this->fetch_twitter_params( array( 'owner_screen_name', 'owner_id', 'since_id', 'max_id', 'count', 'include_entities', 'include_rts' ) );

		//list_id
		$list_id = $this->EE->TMPL->fetch_param( 'list_id', '' );
		//slug
		$slug = $this->EE->TMPL->fetch_param( 'slug', '' );
		if ( empty( $list_id ) && empty( $slug ) )
		{
			$this->log_debug_messages( 'You must specify a list_id= or slug= when using the "lists_statuses" tag.' );
			return $this->EE->TMPL->no_results();
		}

		if ( ! empty( $list_id ) && is_numeric( $list_id ) )
		{
			$params['list_id'] = $list_id;
		}
		else if ( ! empty( $slug ) && ( isset( $params['owner_screen_name'] ) || isset( $params['owner_id'] ) ) )
		{
			$params['slug'] = $slug;
		}

		return $this->get_response( __FUNCTION__, '/lists/statuses.json', $params );
	}

	//---------------------- Accounts ----------------------
	/**
	 * Returns the remaining number of API requests available to the requesting user before the API limit is reached for the current hour. Calls to rate_limit_status do not count against the rate limit. If authentication credentials are provided, the rate limit status for the authenticating user is returned. Otherwise, the rate limit status for the requester's IP address is returned.
	 *
	 * @url https://dev.twitter.com/docs/api/1.1/get/application/rate_limit_status
	 * @return string
	 */
	public function rate_limit_status()
	{
		$params = array();

		$resources = $this->EE->TMPL->fetch_param( 'resources' );
		if ( ! empty( $resources ) )
		{
			$params['resources'] = $resources;
		}

		return $this->get_response( __FUNCTION__, '/application/rate_limit_status.json', $params, true, false );
	}

	/**
	 * Cleans up the redundant keys in order to create more friendly variables.
	 *
	 * @param $data
	 * @return array
	 */
	protected function rate_limit_status_key_cleanup( $data )
	{
		$new = array();
		foreach ( $data as $key => $value )
		{
			$key = str_replace(
				array(
					'resources_',
					'lists_lists_',
					'application_application_',
					'friendships_friendships_',
					'blocks_blocks_',
					'geo_geo_',
					'users_users_',
					'followers_followers_',
					'statuses_statuses_',
					'help_help_',
					'direct_messages_direct_messages_',
					'account_account_',
					'favorites_favorites_',
					'saved_searches_saved_searches_',
					'search_search_',
					'trends_trends_',
				),
				array(
					'',
					'lists_',
					'application_',
					'friendships_',
					'blocks_',
					'geo_',
					'users_',
					'followers_',
					'statuses_',
					'help_',
					'direct_messages_',
					'account_',
					'favorites_',
					'saved_searches_',
					'search_',
					'trends_',
				),
				$key
			);

			$new[$key] = $value;
		}

		unset( $data );
		return $new;
	}

	/**
	 * Returns an HTTP 200 OK response code and a representation of the requesting user if authentication was successful; returns a 401 status code and an error message if not. Use this method to test if supplied user credentials are valid.
	 *
	 * @url https://api.twitter.com/1.1/account/verify_credentials.json
	 * @return string
	 */
	public function verify_credentials()
	{
		return $this->get_response( __FUNCTION__, '/account/verify_credentials.json', array(), true, false );
	}

	//---------------------- Deprecated Tags ----------------------
	/**
	 * Returns the most recent retweets posted by the authenticating user. This tag is deprecated as of API version 1.1.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/retweeted_by_me
	 * @return string
	 */
	public function retweeted_by_me()
	{
		$this->log_debug_messages( 'The retweeted_by_me tag is deprecated as of API version 1.1.' );
		return $this->EE->TMPL->no_results();
	}

	/**
	 * Returns the most recent retweets posted by users the authenticating user follows.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/retweeted_to_me
	 * @return string
	 */
	public function retweeted_to_me()
	{
		$this->log_debug_messages( 'The retweeted_to_me tag is deprecated as of API version 1.1.' );
		return $this->EE->TMPL->no_results();
	}

	/**
	 * Returns the most recent retweets posted by users the specified user follows.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/retweeted_to_user
	 * @return string
	 */
	public function retweeted_to_user()
	{
		$this->log_debug_messages( 'The retweeted_to_user tag is deprecated as of API version 1.1.' );
		return $this->EE->TMPL->no_results();
	}

	/**
	 * Returns the most recent retweets posted by the specified user.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/retweeted_by_user
	 * @return string
	 */
	public function retweeted_by_user()
	{
		$this->log_debug_messages( 'The retweeted_by_user tag is deprecated as of API version 1.1.' );
		return $this->EE->TMPL->no_results();
	}

	/**
	 * Show user objects of up to 100 members who retweeted the status.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/statuses/%3Aid/retweeted_by
	 * @return string
	 */
	public function retweeted_by()
	{
		$this->log_debug_messages( 'The retweeted_by tag is deprecated as of API version 1.1.' );
		return $this->EE->TMPL->no_results();
	}


	/**
	 * Returns the current count of friends, followers, updates (statuses) and favorites of the authenticating user.
	 *
	 * @url https://dev.twitter.com/docs/api/1/get/account/totals
	 * @return string
	 */
	public function totals()
	{
		$this->log_debug_messages( 'The totals tag is deprecated as of API version 1.1.' );
		return $this->EE->TMPL->no_results();
	}

	/**
	 * Can take a Twitter profile image URL of any size, and convert it to a URL of a specific size. The size= parameter can be 'normal', 'bigger', 'mini', or 'original' (the default is 'original'). The src= parameter is also required and can be a valid URL of any size profile image.
	 *
	 * @return string
	 */
	public function profile_image()
	{
		$size = $this->EE->TMPL->fetch_param( 'size', 'original' );
		if ( ! in_array( $size, array( 'normal', 'bigger', 'mini', 'original' ) ) )
		{
			$this->log_debug_messages( 'You must specify an image size.' );
			return $this->EE->TMPL->no_results();
		}

		$src = $this->EE->TMPL->fetch_param( 'src' );
		if ( empty( $src ) || strpos( $src, 'profile_images/' ) === false )
		{
			$this->log_debug_messages( 'You must specify an image source.' );
			return $this->EE->TMPL->no_results();
		}
		if ( strpos( $src, '{' ) !== false )
		{
			$this->log_debug_messages( 'You must specify a valid image source.' );
			return $this->EE->TMPL->no_results();
		}

		//get the original
		$info = pathinfo( $src );
		$filename = $info['filename'];
		if ( preg_match( '@(.*)_(normal|bigger|mini)$@', $filename, $match ) )
		{
			$filename = $match[1];
		}

		//convert to the requested size
		if ( $size != "original" )
		{
			$filename .= '_' . $size;
		}

		//return the image URL
		return $info['dirname'] . '/' . $filename . '.' . $info['extension'];
	}

	//-------------------------------------------- Get Response --------------------------------------------
	/**
	 * This is where most of the work is done. The data is processed and cached or retrieved as necessary.
	 *
	 * @param string $method The method name.
	 * @param string $endpoint The twitter API endpoint.
	 * @param array $params The parameters.
	 * @param bool $single_item Will the expected result be a single item?
	 * @param bool $cache Should the result be cached?
	 * @return string
	 */
	protected function get_response( $method = '', $endpoint = '', $params = array(), $single_item = false, $cache = true )
	{
		//grab the credentials
		$consumer_key = $this->determine_setting( 'consumer_key' );
		$consumer_secret = $this->determine_setting( 'consumer_secret' );
		$oauth_token = $this->determine_setting( 'oauth_token' );
		$oauth_secret = $this->determine_setting( 'oauth_secret' );

		//make sure the credentials are not empty
		if ( empty( $consumer_key ) || empty( $consumer_secret ) || empty( $oauth_token ) || empty( $oauth_secret ) )
		{
			$this->log_debug_messages( 'Your application credentials cannot be empty.' );
			return $this->EE->TMPL->no_results();
		}

		//linking options
		$linking = $this->determine_setting( 'link', 'h|m|u' );

		//show the response instead of parsing the tagdata?
		$show_response = $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'show_response', 'no' ) );

		if ( $cache ) //caching is enabled for this request type
		{
			//create a unique string from the credentials and params
			$unique = $endpoint . $consumer_key . $consumer_secret . $oauth_token . $oauth_secret;
			foreach ( $params as $k => $v )
			{
				$unique .= $k . $v;
			}
			//add the settings to the unique string
			$settings = array();
			$seconds = $this->determine_setting( 'seconds', 300 );
			if ( ! is_numeric( $seconds ) )
			{
				$seconds = 300;
			}
			$settings['seconds'] = $seconds;

			$max_items = ( $single_item && $method != 'search' ) ? 1 : $this->determine_setting( 'max_items', '' );

			if ( ! is_numeric( $max_items ) )
			{
				$max_items = ( isset( $params['count'] ) && is_numeric( $params['count'] ) ) ? $params['count'] : 20;
			}

			if ( $max_items < 1 )
			{
				return $this->EE->TMPL->no_results();
			}

			$settings['max_items'] = $max_items;

			$settings['linking'] = $linking;
			foreach ( $settings as $k => $v )
			{
				$unique .= $k . $v;
			}

			unset( $settings );

			//the raw cache filename
			$cache_raw = 'r_' . md5( $unique );

			//the parsed cache filename
			$cache_parsed = 'p_' . md5( $this->EE->TMPL->tagchunk . $unique );

			//the cache base
			$cache_base = str_replace( array( '\\', '/', ), DIRECTORY_SEPARATOR, $this->remove_duplicate_slashes( APPPATH . 'cache/ce_tweet/' ) );

			//create the main cache directory if needed
			if ( ! @is_dir( rtrim( $cache_base, '/\\' ) ) )
			{
				//try to make the directory with full permissions
				if ( ! @mkdir( $cache_base, 0777, true ) )
				{
					$this->log_debug_messages( "Could not create the cache directory '{$cache_base}'." );
				}
			}

			//current items
			$cached_items = array();

			//check the cache
			$cache_mod_time = $this->check_cache( $cache_base . $cache_parsed );
			if ( $cache_mod_time !== false && is_numeric( $cache_mod_time ) ) //the cached parse file was found
			{
				if ( time() > $cache_mod_time + $seconds ) //the cache time has expired
				{
					if ( ! $single_item || $method == 'search' ) //not a single item, we'll want to retrieve the existing items
					{
						//attempt to get the raw cached items
						$orig = $this->check_cache( $cache_base . $cache_raw, 'file' );

						if ( ! empty( $orig ) )
						{
							//decode the stored JSON data
							$cached_items = json_decode( $orig, true );

							if ( empty( $params['since_id'] ) )
							{
								$since_id = 0;

								if ( $method == 'search' && isset( $cached_items['max_id_str'] ) )
								{
									$since_id = $cached_items['max_id_str'];
								}
								else
								{
									//loop through the items and see if we can get an id string
									foreach ( $cached_items as $current_item )
									{
										if ( isset( $current_item['id_str'] ) && $current_item['id_str'] > $since_id )
										{
											$since_id = $current_item['id_str'];
										}
									}
								}

								if ( ! empty( $since_id ) )
								{
									$params['since_id'] = $since_id;
								}
							}
						}
						unset( $orig );
					}
				}
				else //the parse cache is still good
				{
					//return the previously parsed data
					$this->EE->load->helper( 'file' );

					$content = read_file( $cache_base . $cache_parsed );
					if ( $content === false )
					{
						$this->log_debug_messages( 'The file "' . $cache_base . $cache_parsed . '" could not be read.' );
						return $this->EE->TMPL->no_results();
					}
					return $content;
				}
			}
		}

		//include the classes if needed
		$this->include_classes();

		//create the EpiTwitter class
		try
		{
			$twitter = new EpiTwitter( $consumer_key, $consumer_secret, $oauth_token, $oauth_secret );
			$twitter->useApiVersion( '1.1' );
			$twitter->useApiUrl( 'https://api.twitter.com' );
			$twitter->setTimeout( 30, 10 );

			//run the method
			$response = $twitter->get( $endpoint, $params );
			$data = $response->response;
		}
		catch ( EpiTwitterException $e )
		{
			$this->log_debug_messages( 'An exception occurred: ' . $e->getCode() . ' "' . $e->getMessage() . '"' );
			return $this->EE->TMPL->no_results();
		}
		catch ( Exception $e )
		{
			$this->log_debug_messages( 'An unexpected exception occurred: ' . $e->getCode() . ' "' . $e->getMessage() . '"' );
			return $this->EE->TMPL->no_results();
		}

		//prevent variable conflict in oembed
		if ( $method == 'oembed' )
		{
			if (isset($data['version']))
			{
				$data['provider_version'] = $data['version'];
				unset( $data['version']);
			}
		}

		//ensure that data is an array, even if it is an empty one
		if ( empty( $data ) )
		{
			$data = array();
		}
		else if ( ! is_array( $data ) )
		{
			$data = (array) $data;
		}

		if ( $cache ) //merge the new data with the cache data
		{
			if ( count( $data ) > 0 )
			{
				if ( $method == 'search' )
				{
					//combine the new and cached statuses, being careful to avoid duplicates
					if ( isset( $cached_items['statuses'] ) && is_array( $cached_items['statuses'] ) && isset( $data['statuses'] ) && is_array( $data['statuses'] ) )
					{
						//array of unique status ids
						$temp_ids = array();

						//populate the unique status ids array
						foreach( $data['statuses'] as $index => $status )
						{
							$temp_ids[] = $status['id_str'];
						}

						//add in the cached items if they are not alreay in the data statuses array
						foreach( $cached_items['statuses'] as $index => $status )
						{
							if ( ! in_array( $status['id_str'], $temp_ids ) )
							{
								array_push( $data['statuses'], $status );
							}
						}
					}

					//make sure the array doesn't exceed the maximum number of items (only slice if not a single item)
					if ( isset( $data['statuses'] ) && is_array( $data['statuses'] ) && count( $data['statuses'] ) > $max_items )
					{
						$data['statuses'] = array_slice( $data['statuses'], 0, $max_items );
					}
				}
				else
				{
					//merge the new data with the cached items
					$data = array_merge( $data, $cached_items );

					//make sure the array doesn't exceed the maximum number of items (only slice if not a single item)
					if ( ! $single_item && count( $data ) > $max_items )
					{
						$data = array_slice( $data, 0, $max_items );
					}
				}

				$this->EE->load->helper( 'file' );

				//encode the data into json
				$encoded = json_encode( $data );

				//save the json to the raw file
				if ( ! write_file( $cache_base . $cache_raw, $encoded ) )
				{
					$this->log_debug_messages( 'The raw cache file "' . $cache_base . $cache_raw . '" could not be written.' );
				}
				unset( $encoded );
			}
			else
			{
				$data = (array) $cached_items;
			}
		}

		if ( count( $data ) == 0 ) //there's no data
		{
			return $this->EE->TMPL->no_results();
		}

		//determine if the twitter text should be linked. h => hashes, m => mentions, u => urls
		$this->linking = array(
			'h' => strpos( $linking, 'h' ) !== false,
			'm' => strpos( $linking, 'm' ) !== false,
			'u' => strpos( $linking, 'u' ) !== false
		);

		//get the var prefix
		$tag_parts = $this->EE->TMPL->tagparts;
		$this->var_prefix = ( is_array( $tag_parts ) && isset( $tag_parts[2] ) ) ? $tag_parts[2] . ':' : '';

		//give a total status count for the search results
		if ( $method == 'search' )
		{
			$data['total_status_count'] = count( $data['statuses'] );
			foreach ( $data['statuses'] as $index => $status )
			{
				$data['statuses'][$index]['status_count'] = $index + 1;
			}
		}

		//make the response data EE friendly
		$data = $this->make_array_ee_friendly( $data );

		//make sure the search method gets rid of the extra statuses tags
		if ( $method == 'search' && ! isset( $data['statuses'] ) )
		{
			$data['statuses'] = array();
		}

		//special key cleanup
		if ( $method == 'rate_limit_status' )
		{
			$data = $this->rate_limit_status_key_cleanup( $data );
		}

		//show the response if requested in the tag
		if ( $show_response )
		{
			ob_start();
			print_r( $data );
			$buffer = ob_get_contents();
			ob_end_clean();
			return '<pre>' . $buffer . '</pre>';
		}

		//get the tagdata
		$tagdata = $this->EE->TMPL->tagdata;

		//this fixes a bug where date variables are not parsed when identical date variables in different tags are used in the same template
		$this->EE->TMPL->_match_date_vars( $tagdata );

		$count = count( $data );
		if ( $count >= 1  )
		{
			if ( isset( $data['0'] ) )
			{
				if ( ! empty( $this->var_prefix ) )
				{
					//include a prefixed count variable
					foreach ( $data as $index => $datum )
					{
						$data[$index][$this->var_prefix . 'count'] = $index + 1;
					}
				}

				$final = $this->EE->TMPL->parse_variables( $tagdata, $data );
			}
			else //single row
			{
				$final = $this->EE->TMPL->parse_variables_row( $tagdata, $data );
			}

			if ( $cache )
			{
				$this->EE->load->helper( 'file' );
				//save the parsed tag to the file
				if ( ! write_file( $cache_base . $cache_parsed, $final ) )
				{
					$this->log_debug_messages( 'The parsed cache file "' . $cache_base . $cache_parsed . '" could not be written.' );
				}
			}

			return $final;
		}
		else
		{
			return $this->EE->TMPL->no_results();
		}
	}

	//-------------------------------------------- Helper Methods --------------------------------------------
	/**
	 * Include the required classes.
	 */
	protected function include_classes()
	{
		//include the classes if needed
		if ( ! class_exists( 'EpiCurl' ) )
		{
			require PATH_THIRD . 'ce_tweet/libraries/EpiCurl.php';
		}
		if ( ! class_exists( 'EpiOAuth' ) )
		{
			require PATH_THIRD . 'ce_tweet/libraries/EpiOAuth.php';
		}
		if ( ! class_exists( 'EpiTwitter' ) )
		{
			require PATH_THIRD . 'ce_tweet/libraries/EpiTwitter.php';
		}
	}

	/**
	 * Checks if a path exists.
	 *
	 * @param $path
	 * @param string $type can be 'mtime' (just get the modified time), or 'file' (get the file contents).
	 * @return bool|int Returns false if the file is not found, returns the file's last modification time if it was successfully found.
	 */
	protected function check_cache( $path, $type = 'mtime' )
	{
		if ( ! file_exists( $path ) ) //the file doesn't exist
		{
			$this->log_debug_messages( 'The cache file "' . $path . '" does not exist.' );
			return false;
		}
		else //the file exists
		{
			$this->log_debug_messages( 'The cache file "' . $path . '" exists.' );

			if ( $type == 'mtime' ) //return get the modified time
			{
				$mod_time = @filemtime( $path );

				if ( ! $mod_time )
				{
					$this->log_debug_messages( 'The last modified time could not be found for the cache file "' . $path . '".' );
					return false;
				}

				return $mod_time;
			}
			else //return the file contents
			{
				$this->EE->load->helper( 'file' );

				$content = read_file( $path );
				if ( $content === false )
				{
					$this->log_debug_messages( 'The file "' . $path . '" could not be read.' );
					return false;
				}

				return $content;
			}
		}
	}


	/**
	 * Fetch the designated parameters.
	 *
	 * @param array $which An array of parameter values to retrieve.
	 * @return array The parameter array (associative).
	 */
	protected function fetch_twitter_params( $which = array() )
	{
		//setup the parameters return array
		$params = array();

		//user_id
		if ( in_array( 'user_id', $which ) )
		{
			$user_id = $this->EE->TMPL->fetch_param( 'user_id', '' );

			if ( ! empty( $user_id ) && is_numeric( $user_id ) )
			{
				$params['user_id'] = $user_id;
			}
		}

		//owner_id
		if ( in_array( 'owner_id', $which ) )
		{
			$owner_id = $this->EE->TMPL->fetch_param( 'owner_id', '' );

			if ( ! empty( $owner_id ) && is_numeric( $owner_id ) )
			{
				$params['owner_id'] = $owner_id;
			}
		}

		//screen_name
		if ( in_array( 'screen_name', $which ) )
		{
			$screen_name = $this->EE->TMPL->fetch_param( 'screen_name', '' );

			if ( ! empty( $screen_name ) )
			{
				$params['screen_name'] = $screen_name;
			}
		}

		//owner_screen_name
		if ( in_array( 'owner_screen_name', $which ) )
		{
			$owner_screen_name = $this->EE->TMPL->fetch_param( 'owner_screen_name', '' );

			if ( ! empty( $owner_screen_name ) )
			{
				$params['owner_screen_name'] = $owner_screen_name;
			}
		}

		//count - defaults to 20
		if ( in_array( 'count', $which ) )
		{
			$params['count'] = $this->EE->TMPL->fetch_param( 'count', 20 );
		}

		//since_id
		if ( in_array( 'since_id', $which ) )
		{
			$since_id = $this->EE->TMPL->fetch_param( 'since_id' );
			if ( ! empty( $since_id ) && is_numeric( $since_id ) )
			{
				$params['since_id'] = $since_id;
			}
		}

		//max_id
		if ( in_array( 'max_id', $which ) )
		{
			$max_id = $this->EE->TMPL->fetch_param( 'max_id' );
			if ( ! empty( $max_id ) && is_numeric( $since_id ) )
			{
				$params['max_id'] = $max_id;
			}
		}

		//trim_user
		if ( in_array( 'trim_user', $which ) )
		{
			$trim_user = $this->EE->TMPL->fetch_param( 'trim_user', 'no' );
			$params['trim_user'] = $this->ee_string_to_bool( $trim_user );
		}

		//include_rts
		if ( in_array( 'include_rts', $which ) )
		{
			$params['include_rts'] = $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'include_rts', 'no' ) );
		}

		//include_entities
		if ( in_array( 'include_entities', $which ) )
		{
			$params['include_entities'] = $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'include_entities', 'yes' ) );
		}

		//exclude_replies
		if ( in_array( 'exclude_replies', $which ) )
		{
			$params['exclude_replies'] = $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'exclude_replies', 'no' ) );
		}

		//contributor_details
		if ( in_array( 'contributor_details', $which ) )
		{
			$params['contributor_details'] = $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'contributor_details', 'yes' ) );
		}

		return $params;
	}

	/**
	 * Simple method to log an array of debug messages to the EE Debug console.
	 *
	 * @param array $messages The debug messages.
	 * @return void
	 */
	protected function log_debug_messages( $messages = array() )
	{
		if ( is_string( $messages ) )
		{
			$messages = array( $messages );
		}

		foreach ( $messages as $message )
		{
			$this->EE->TMPL->log_item( '&nbsp;&nbsp;***&nbsp;&nbsp;CE Tweet debug: ' . $message );
		}
	}

	/**
	 * Determines the given setting by checking for the param, and then for the global var, and then for the config item.
	 * @param string $name The name of the parameter. The string 'ce_tweet_' will automatically be prepended for the global and config setting checks.
	 * @param string $default The default setting value
	 * @return string The setting value if found, or the default setting if not found.
	 */
	protected function determine_setting( $name, $default = '' )
	{
		$long_name = 'ce_tweet_' . $name;
		if ( isset( $this->EE->TMPL ) && $this->EE->TMPL->fetch_param( $name ) !== false ) //param
		{
			$default = $this->EE->TMPL->fetch_param( $name );
		}
		else if ( isset( $this->EE->config->_global_vars[ $long_name ] ) && $this->EE->config->_global_vars[ $long_name ] !== false ) //first check global array
		{
			$default = $this->EE->config->_global_vars[ $long_name ];
		}
		else if ( $this->EE->config->item( $long_name ) !== false ) //then check config
		{
			$default = $this->EE->config->item( $long_name );
		}

		return $default;
	}

	/**
	 * Cleanup array key (variable) values.
	 *
	 * @param $key
	 * @return mixed
	 */
	protected function key_cleanup( $key )
	{
		$key = preg_replace( '@(_{2,})|(\/)|(_:)|(\s+)@', '_', $key );

		return $key;
	}

	/**
	 * Convert an array into an EE parser-friendly array, so that the associative array from the JSON data can be magically transformed into EE variables.
	 *
	 * @param array $data
	 * @param string $prefix
	 * @return array
	 */
	protected function make_array_ee_friendly( $data, $prefix = '' )
	{
		//the return array
		$result = array();

		if ( isset( $data['text'] ) )
		{
			$data['text_raw'] = $data['text'];
			$data['text'] = $this->cleanup_tweet_text( $data ); //cleaned-up text

			if ( $prefix === '' )
			{
				$data['is_retweet'] = isset( $data['retweeted_status'] ) ? 1 : 0;
			}
		}

		foreach ( $data as $key => $value ) //loop through the key => value pairs
		{
			//make sure orphaned tag pairs have a value
			if ( is_int( $key ) && isset( $data[0] ) && ! is_array( $value ) )
			{
				$value = array( 'value' => $value );
			}

			if ( is_array( $value ) ) //the value is an array
			{
				if ( is_int( $key ) && isset( $data[0] ) ) //numeric key
				{
					if ( ! empty( $prefix ) )
					{
						$result[  $this->var_prefix . rtrim( $prefix, '_' )][ $key ] = $this->make_array_ee_friendly( $value );
					}
					else
					{
						$result[ $key ] = $this->make_array_ee_friendly( $value );
					}
				}
				else //non-numeric key
				{
					$key = $this->key_cleanup( $prefix . $key );
					$result = array_merge( $result, $this->make_array_ee_friendly( $value, $key . '_' ) );
				}
			}
			else //the value is not an array
			{
				if ( is_int( $key ) && isset( $data[0] ) ) //numeric key
				{
					if ( ! empty( $prefix ) )
					{
						$result[ $this->var_prefix . rtrim( $prefix, '_' ) ][$key] = $value;
					}
					else
					{
						$result[ $key ] = $value;
					}
				}
				else //non-numeric key
				{
					$key = $this->key_cleanup( $this->var_prefix . $prefix . $key );

					if ( substr( $key, -3, 3 ) == '_at' ) //date string
					{
						//determine timestamp
						$result[$key] = strtotime( $value );
						//relative date string
						$result[$key . '_relative'] = $this->relative( $value );
					}
					else
					{
						$result[$key] = $value;
					}
				}
			}
		}

		return $result;
	}

	/**
	 * Cleans up tweet text to keep EE sites safe. Also parses mentions, hashtags, and URLs, if applicable.
	 *
	 * @param array $data
	 * @internal param string $text
	 * @return string
	 */
	protected function cleanup_tweet_text( $data )
	{
		if ( ! isset( $data['text'] ) )
		{
			return '';
		}

		$text = $data['text'];

		//keep EE sites safe by encoding curly brackets and braces to HTML entities
		$text = str_replace( array( '{', '}', '<', '>' ), array( '&#123;', '&#125;', '&lt;', '&gt;' ), $text );

		if ( isset( $data['entities'] ) ) //we have entities
		{
			$search = array();
			$replace = array();

			if ( $this->linking['u'] ) //replace URLs
			{
				foreach ( $data['entities']['urls'] as $index => $urls )
				{
					$search[] = $urls['url'];
					$replace[] = '<a href="' . $urls['url'] . '" title="' . $urls['expanded_url'] . '" target="_blank"><span>' . ( isset( $urls['display_url'] ) ? $urls['display_url'] : $urls['url'] ) . '</span></a>';
				}

				if ( isset( $data['entities']['media'] ) )
				{
					foreach ( $data['entities']['media'] as $index => $urls )
					{
						$search[] = $urls['url'];
						$replace[] = '<a href="' . ( isset( $urls['expanded_url'] ) ? $urls['expanded_url'] : $urls['url'] ) . '" title="' . $urls['expanded_url'] . '" target="_blank"><span>' . ( isset( $urls['display_url'] ) ? $urls['display_url'] : $urls['url'] ) . '</span></a>';
					}
				}
			}

			if ( $this->linking['m'] ) //replace mentions
			{
				foreach ( $data['entities']['user_mentions'] as $index => $mention )
				{
					$search[] = '@' . $mention['screen_name'];
					$replace[] = '<a href="http://twitter.com/' . $mention['screen_name'] . '" title="' . $mention['name'] . '" target="_blank">@<span>' . $mention['screen_name'] . '</span></a>';
				}
			}

			if ( $this->linking['h'] ) //replace hashtags
			{
				foreach ( $data['entities']['hashtags'] as $index => $tag )
				{
					$search[] = '#' . $tag['text'];
					$replace[] = '<a href="http://twitter.com/search/%23' . $tag['text'] . '" target="_blank">#<span>' . $tag['text'] . '</span></a>';
				}
			}

			if ( count( $search ) > 0 ) //there are replacements to be made
			{
				//although preg_replace is a little slower, it has a count parameter that ensure that the replace is only happening once, which minimized risk of accidental mangling
				foreach ( $search as $i => $s )
				{
					$search[$i] = '~' . preg_quote( $s, '~' ) . '~';
				}
				$text = preg_replace( $search, $replace, $text, 1 );
			}
		}
		else //old school twitterfy regex slightly modified from http://www.snipe.net/2009/09/php-twitter-clickable-links/
		{
			if ( $this->linking['u'] ) //replace URLs
			{
				$text = preg_replace( '#(^|[\n ])([\w]+?://[\w]+[^ "\n\r\t< ]*)#', '\\1<a href="\\2" target="_blank"><span>\\2</span></a>', $text );
				$text = preg_replace( '#(^|[\n ])((www|ftp)\.[^ "\t\n\r< ]*)#', '\\1<a href="http://\\2" target="_blank"><span>\\2</span></a>', $text );
			}

			if ( $this->linking['m'] ) //replace mentions
			{
				$text = preg_replace( '/@(\w+)/', '<a href="http://www.twitter.com/\\1" target="_blank">@<span>\\1</span></a>', $text );
			}

			if ( $this->linking['h'] ) //replace hashtags
			{
				$text = preg_replace( '/#(\w+)/', '<a href="http://search.twitter.com/search?q=\\1" target="_blank">#<span>\\1</span></a>', $text );
			}
		}
		return $text;
	}

	/**
	 * Little helper method to convert parameters to a boolean value.
	 *
	 * @param $string
	 * @return bool
	 */
	protected function ee_string_to_bool( $string )
	{
		return ( $string == 'y' || $string == 'yes' || $string == 'on' || $string === true );
	}

	/**
	 * Returns an English representation of a past date within the last month. Taken from the simple-php-framework <http://www.phpkode.com/source/p/simple-php-framework/includes/functions.inc.php> which in turn was ported from JavaScript from Pretty JS <http://ejohn.org/files/pretty.js>
	 * todo add language files for the relative time strings
	 * @license MIT License
	 * @param $timestamp
	 * @return string
	 */
	public function relative( $timestamp = null )
	{
		if ( empty( $timestamp ) )
		{
			$timestamp = $this->EE->TMPL->fetch_param( 'date' );

			if ( empty( $timestamp ) )
			{
				return $this->EE->TMPL->no_results();
			}
		}

		if ( ! ctype_digit( $timestamp ) )
		{
			$timestamp = strtotime( $timestamp );
			if ( $timestamp == false )
			{
				return $this->EE->TMPL->no_results();
			}
		}

		$diff = time() - $timestamp;
		if ( $diff == 0 )
		{
			return 'now';
		}
		elseif ( $diff > 0 )
		{
			$day_diff = floor( $diff / 86400 );
			if ( $day_diff == 0 )
			{
				if ( $diff < 60 )
				{
					return 'just now';
				}
				if ( $diff < 120 )
				{
					return '1 minute ago';
				}
				if ( $diff < 3600 )
				{
					return floor( $diff / 60 ) . ' minutes ago';
				}
				if ( $diff < 7200 )
				{
					return '1 hour ago';
				}
				if ( $diff < 86400 )
				{
					return floor( $diff / 3600 ) . ' hours ago';
				}
			}
			if ( $day_diff == 1 )
			{
				return 'Yesterday';
			}
			if ( $day_diff < 7 )
			{
				return $day_diff . ' days ago';
			}
			if ( $day_diff < 31 )
			{
				return ceil( $day_diff / 7 ) . ' weeks ago';
			}
			if ( $day_diff < 60 )
			{
				return 'last month';
			}
			$ret = date( 'F Y', $timestamp );
			return ( $ret == 'December 1969' ) ? '' : $ret;
		}
		else
		{
			$diff = abs( $diff );
			$day_diff = floor( $diff / 86400 );
			if ( $day_diff == 0 )
			{
				if ( $diff < 120 )
				{
					return 'in a minute';
				}
				if ( $diff < 3600 )
				{
					return 'in ' . floor( $diff / 60 ) . ' minutes';
				}
				if ( $diff < 7200 )
				{
					return 'in an hour';
				}
				if ( $diff < 86400 )
				{
					return 'in ' . floor( $diff / 3600 ) . ' hours';
				}
			}
			if ( $day_diff == 1 )
			{
				return 'Tomorrow';
			}
			if ( $day_diff < 4 )
			{
				return date( 'l', $timestamp );
			}
			if ( $day_diff < 7 + ( 7 - date( 'w' ) ) )
			{
				return 'next week';
			}
			if ( ceil( $day_diff / 7 ) < 4 )
			{
				return 'in ' . ceil( $day_diff / 7 ) . ' weeks';
			}
			if ( date( 'n', $timestamp ) == date( 'n' ) + 1 )
			{
				return 'next month';
			}
			$ret = date( 'F Y', $timestamp );
			return ( $ret == 'December 1969' ) ? '' : $ret;
		}
	}

	//-------------------------------------------- Buttons --------------------------------------------
	/**
	 * Generates a Follow Button.
	 *
	 * @url https://dev.twitter.com/docs/follow-button
	 * @return string
	 */
	public function follow_button()
	{
		//screen_name to follow
		$screen_name = $this->EE->TMPL->fetch_param( 'screen_name' );

		if ( empty( $screen_name ) )
		{
			$this->log_debug_messages( 'The follow button must specify the Twitter user to follow in the tagdata.' );
			return $this->EE->TMPL->no_results();
		}

		//the parameters
		$params = array();

		$screen_name = ltrim(trim($screen_name), '@');

		//count display
		if ( ! $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'show_count', 'no' ) ) ) //if show count is set to "yes"
		{
			$params['show-count'] = 'false';
		}

		//button size
		if ( $this->EE->TMPL->fetch_param( 'size', 'medium' ) == 'large' )
		{
			$params['size'] = 'large';
		}

		//language
		if ( $lang = $this->EE->TMPL->fetch_param( 'lang' ) )
		{
			$params['lang'] = $lang;
		}

		//align
		$align = $this->EE->TMPL->fetch_param( 'align' );
		if ( ! empty( $align ) && in_array( $align, array( 'left', 'right') ) )
		{
			$params['align'] = $align;
		}

		//width
		$width = $this->EE->TMPL->fetch_param( 'width' );
		if ( ! empty( $width ) )
		{
			if ( is_numeric( $width ) )
			{
				$width .= 'px';
			}
			else if ( strpos($width,'px') !== false && strpos( $width, '%' ) !== false )
			{
				$width = false;
			}

			if ( $width )
			{
				$params['width'] = $width;
			}
		}

		//show-screen-name
		$show_screen_name = $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'show_screen_name', 'yes' ) );
		if ( ! $show_screen_name )
		{
			$params['show-screen-name'] = 'false';
		}

		//opt out
		if ( $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'dnt_opt_out', 'yes' ) ) )
		{
			$params['dnt'] = 'true';
		}

		//build the button
		if ( ! $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'iframe', 'no' ) ) ) //the JavaScript version
		{
			$button = '<a href="https://twitter.com/' . $screen_name . '" class="twitter-follow-button" ';

			//construct the parameters
			$parameters = '';
			foreach ( $params as $param => $value )
			{
				$value = htmlspecialchars( $value );
				$parameters .= ( strpos( $value, '"' ) === false) ? "data-{$param}=\"{$value}\" " : "data-{$param}='{$value}' ";
			}

			$button .= rtrim( $parameters ) . '>Follow @' . $screen_name . '</a>';
			return $button;
		}
		else //iframe
		{
			//these params are unsupported for the iframe
			unset( $params['size'], $params['width'], $params['align'] );

			//screen_name
			$params['screen_name'] = $screen_name;

			//construct the parameters
			$parameters = array();
			foreach ( $params as $param => $value )
			{
				$parameters[] = str_replace( '-', '_', $param ) . '=' . urlencode( $value );
			}
			$parameters = implode( '&', $parameters );

			//build the iframe
			$iframe = '<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/follow_button.html?'. $parameters . '"';

			//add in any styles
			$style = $this->EE->TMPL->fetch_param( 'style' );
			if ( $style )
			{
				$iframe .= ( strpos( $style, '"' ) === false) ? " style=\"{$style}\"" : " style='{$style}'";
			}

			//close the tag
			$iframe .= '></iframe>';

			return $iframe;
		}
	}

	/**
	 * Generates a Tweet Button.
	 *
	 * @url https://dev.twitter.com/docs/tweet-button
	 * @return string
	 */
	public function tweet_button()
	{
		//the parameters
		$params = array();

		//screen_name
		$screen_name = $this->EE->TMPL->fetch_param( 'screen_name' );

		//hash tag
		$hashtag = $this->EE->TMPL->fetch_param( 'hashtag' );

		//text
		$text = trim( $this->EE->TMPL->tagdata );
		if ( ! empty( $text ) )
		{
			$params[ 'text' ] = $text;
		}

		//count box position
		$count = $this->EE->TMPL->fetch_param( 'count' );
		if ( in_array( $count, array( 'none', 'horizontal', 'vertical' ) ) )
		{
			$params['count'] = $count;
		}

		//button size
		if ( $this->EE->TMPL->fetch_param( 'size', 'medium' ) == 'large' )
		{
			$params['size'] = 'large';
		}

		//opt out
		if ( $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'dnt_opt_out', 'yes' ) ) )
		{
			$params['dnt'] = 'true';
		}

		//misc params
		$temp = array( 'url', 'count_url', 'via', 'related', 'hashtags', 'lang' );
		foreach ( $temp as $param )
		{
			$value = $this->EE->TMPL->fetch_param( $param );
			if ( ! empty( $value ) )
			{
				$params[ $param ] = $value;
			}
		}

		//build the button
		if ( ! $this->ee_string_to_bool( $this->EE->TMPL->fetch_param( 'iframe', 'no' ) ) ) //the JavaScript version
		{
			$button = '';
			$button_end = '';

			if ( ! empty( $screen_name ) ) //Mention (Tweet To) Button
			{
				$screen_name = ltrim(trim($screen_name), '@');
				$button .= '<a href="https://twitter.com/intent/tweet?screen_name=' . urlencode( $screen_name ) . '" class="twitter-mention-button" ';
				$button_end .= '>Tweet to @' . $screen_name . '</a>';
			}
			else if ( ! empty( $hashtag ) ) //Hashtag Button
			{
				$hashtag = ltrim(trim($hashtag), '#');
				$button .= '<a href="https://twitter.com/intent/tweet?button_hashtag=' . urlencode( $hashtag ) . '" class="twitter-hashtag-button" ';
				$button_end .= '>Tweet #' . $hashtag . '</a>';
			}
			else //Tweet (Share) Button
			{
				$button .= '<a href="https://twitter.com/share" class="twitter-share-button" ';
				$button_end .= '>Tweet</a>';
			}

			//construct the parameters
			$parameters = '';
			foreach ( $params as $param => $value )
			{
				$value = htmlspecialchars( $value );
				$parameters .= ( strpos( $value, '"' ) === false) ? "data-{$param}=\"{$value}\" " : "data-{$param}='{$value}' ";
			}

			//return the button
			$button .= rtrim( $parameters ) . $button_end;
			return $button;
		}
		else //iframe
		{
			//these params are unsupported for the iframe
			unset( $params['size'], $params['width'], $params['align'] );

			if ( ! empty( $screen_name ) ) //Mention (Tweet To) Button
			{
				$screen_name = ltrim(trim($screen_name), '@');
				$params[ 'screen_name' ] = $screen_name;
				$params[ 'type' ] = 'mention';
			}
			else if ( ! empty( $hashtag ) ) //Hashtag Button
			{
				$hashtag = ltrim(trim($hashtag), '#');
				$params[ 'button_hashtag' ] = $hashtag;
				$params[ 'type' ] = 'hashtag';
			}

			//construct the parameters
			$parameters = array();
			foreach ( $params as $param => $value )
			{
				$parameters[] = str_replace( '-', '_', $param ) . '=' . urlencode( $value );
			}
			$parameters = implode( '&', $parameters );

			//build the iframe
			$iframe = '<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html?'. $parameters . '"';

			//add in any styles
			$style = $this->EE->TMPL->fetch_param( 'style' );
			if ( $style )
			{
				$iframe .= ( strpos( $style, '"' ) === false) ? " style=\"{$style}\"" : " style='{$style}'";
			}

			//close the tag
			$iframe .= '></iframe>';

			return $iframe;
		}
	}

	//-------------------------------------------- Scripts --------------------------------------------
	/**
	 * Outputs the web intents script.
	 *
	 * @url https://dev.twitter.com/docs/intents
	 * @return string
	 */
	public function intents_script()
	{
		$type = $this->EE->TMPL->fetch_param( 'type', 'async' );

		if ( $type == 'sync' )
		{
			return '<script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>';
		}
		else if ( $type == 'url' )
		{
			return 'https://platform.twitter.com/widgets.js';
		}

		//async or unexpected
		return '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
	}

	//-------------------------------------------- Action Methods (Called from Fieldtype) --------------------------------------------

	/**
	 * Gets the response for the AJAX methods. This is different than the get_response method, because there is no TMPL object to call on.
	 *
	 * @param string $endpoint
	 * @param string $params
	 * @return array
	 */
	protected function get_response_for_ajax_methods( $endpoint, $params, $callback )
	{
		//grab the credentials
		$consumer_key = $this->determine_setting( 'consumer_key' );
		$consumer_secret = $this->determine_setting( 'consumer_secret' );
		$oauth_token = $this->determine_setting( 'oauth_token' );
		$oauth_secret = $this->determine_setting( 'oauth_secret' );

		//make sure the credentials are not empty
		if ( empty( $consumer_key ) || empty( $consumer_secret ) || empty( $oauth_token ) || empty( $oauth_secret ) )
		{
			$this->return_ajax_response( false, 'Your application credentials cannot be empty.', $callback );
		}

		//include the classes if needed
		$this->include_classes();

		//create the EpiTwitter class
		try
		{
			$twitter = new EpiTwitter( $consumer_key, $consumer_secret, $oauth_token, $oauth_secret );

			$twitter->useApiVersion( '1.1' );
			$twitter->useApiUrl( 'https://api.twitter.com' );
			$twitter->setTimeout( 30, 10 );

			//run the method
			$response = $twitter->get( $endpoint, $params );
			$data = $response->response;
		}
		catch ( EpiTwitterException $e )
		{
			$this->return_ajax_response( false, 'An exception occurred: "' . $e->getMessage() . '"', $callback );
		}
		catch ( Exception $e )
		{
			$this->return_ajax_response( false, 'An unexpected exception occurred: "' . $e->getMessage() . '"', $callback );
		}

		//ensure that data is an array, even if it is an empty one
		if ( empty( $data ) )
		{
			$data = array();
		}
		else if ( ! is_array( $data ) )
		{
			$data = (array) $data;
		}

		return $data;
	}

	/**
	 * Outputs the response. For use with the AJAX methods.
	 *
	 * @param bool $success
	 * @param string $message
	 * @param string $callback
	 */
	protected function return_ajax_response( $success = true, $message = '', $callback = '' )
	{
		if ( empty( $callback ) ) //if the callback is empty, no reason to proceed
		{
			exit();
		}

		//ajax header
		header('Content-type: application/json');

		$response = array(
			'success' => $success,
			'message' => $message
		);

		echo $callback . '(' . json_encode( $response ) . ')';
		exit();
	}

	/**
	 * Integrates with the fieldtype to perform AJAX searches.
	 *
	 * @return string
	 */
	public function ajax_search()
	{
		//get the callback and check it
		$callback = $this->EE->input->get( 'callback', true );
		$this->check_ajax_request( $callback );

		//get the params
		$query = $this->EE->input->get('q');
		$results_per_page = $this->EE->input->get('count');
		$max_id = $this->EE->input->get('max_id');
		$since_id = $this->EE->input->get('since_id');

		//if params are empty, quit now
		if ( empty( $query ) ||  empty( $results_per_page ) )
		{
			$this->return_ajax_response( false, 'Incorrect parameters.', $callback );
		}

		$params = array(
			'q' => $query,
			'count' => $results_per_page,
			'result_type' => 'recent'
		);

		if ( ! empty( $max_id ) )
		{
			$params['max_id'] = $max_id;
		}

		if ( ! empty( $since_id ) )
		{
			$params['since_id'] = $since_id;
		}

		//get data
		$data = $this->get_response_for_ajax_methods( '/search/tweets.json', $params, 'search', $callback );
		//return response
		$this->return_ajax_response( true, $data, $callback );
	}

	/**
	 * Integrates with the fieldtype to get the rate limit status.
	 *
	 * @return array
	 */
	public function ajax_rate_limit()
	{
		//get the callback and check it
		$callback = $this->EE->input->get( 'callback', true );
		$this->check_ajax_request( $callback );

		//set up the params
		$params = array(
			'resources' => 'search'
		);

		//get the data
		$data = $this->get_response_for_ajax_methods( '/application/rate_limit_status.json', $params, 'rate_limit', $callback );

		//cut out some of the extra levels
		$data = $data['resources']['search']['/search/tweets'];

		//return response
		$this->return_ajax_response( true, $data, $callback );
	}

	/**
	 * Integrates with the fieldtype to show a Tweet.
	 *
	 * @return array|string
	 */
	public function ajax_show()
	{
		//get the callback and check it
		$callback = $this->EE->input->get( 'callback', true );
		$this->check_ajax_request( $callback );

		//set up the parameters
		$id = $this->EE->input->get('tweet_id');
		if ( empty( $id ) )
		{
			$this->return_ajax_response( false, 'Incorrect parameters.', $callback );
		}

		//set up the params
		$params = array(
			'id' => $id
		);

		//get the data
		$data = $this->get_response_for_ajax_methods( '/statuses/show/' . $id . '.json', $params, 'show', $callback );
		//return response
		$this->return_ajax_response( true, $data, $callback );
	}

	/**
	 * Makes sure the callback is not malicious and that the secret matches.
	 *
	 * @param string $callback
	 */
	protected function check_ajax_request( $callback )
	{
		if ( empty( $callback ) || preg_match( '/\W/', $callback ) ) //if the callback contains a non-word character (possible XSS attack), let's bail
		{
			header('HTTP/1.1 400 Bad Request');
			exit();
		}
	}

	/**
	 * Removes double slashes, except when they are preceded by ':', so that 'http://', etc are preserved.
	 *
	 * @param string $str The string from which to remove the double slashes.
	 * @return string The string with double slashes removed.
	 */
	private function remove_duplicate_slashes( $str )
	{
		return preg_replace( '#(?<!:)//+#', '/', $str );
	}
}
/* End of file */