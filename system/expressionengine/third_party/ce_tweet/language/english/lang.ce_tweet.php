<?php

$lang = array(
	//required for the module
	'ce_tweet_module_name' => 'CE Tweet',
	'ce_tweet_module_description' => 'Twitter for templates and entries',

	//used in the global settings
	'ce_tweet_results_per_page_label' => 'Maximum search results to display at one time? Must be between 1-100, inclusively. The default is 10.',
	'ce_tweet_selected_heading' => 'Selected Tweets:',
	'ce_tweet_search_heading' => 'Search Twitter (<a href="https://dev.twitter.com/docs/using-SEARCH" target="_blank">learn more</a>):',

	//fieldtype javascript
	'ce_tweet_connection_problem' => 'There was a problem connecting to Twitter. The Twitter fields will be temporarily disabled.',
	'ce_tweet_moment_format' => 'h:mm A, MMM Do (Z)',
	'ce_tweet_rate_message' => 'You have {remaining} out of {limit} hits left on your Twitter search limit. Your limit will reset again {moment_relative} ({moment_fixed}).',
	'ce_tweet_rate_message_no_hits' => ' The Twitter fields will be temporarily disabled.',
	'ce_tweet_dialog_message' => '<div id="dialog-confirm"><p>There was an error retrieving the tweet with an id of \\\\\'{tweet_id}\\\\\'. Would you like to remove it from your saved tweets?</p></div>',
	'ce_tweet_dialog_title' => 'Tweet Not Found',
	'ce_tweet_dialog_button_keep' => 'Keep the Tweet',
	'ce_tweet_dialog_button_remove' => 'Remove the Tweet',
	'ce_tweet_load_more' => 'Load More'
);