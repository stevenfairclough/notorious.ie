<?php

if ( ! defined('CE_TWEET_NAME'))
{
	define('CE_TWEET_NAME', 'CE Tweet');
	define('CE_TWEET_CLASS', 'Ce_tweet');
	define('CE_TWEET_VERSION', '1.3.7');
	define('CE_TWEET_DOCS', 'http://www.causingeffect.com/software/expressionengine/ce-tweet');
}

$config['name'] = CE_TWEET_NAME;
$config['version'] = CE_TWEET_VERSION;
$config['nsm_addon_updater']['versions_xml'] = 'http://www.causingeffect.com/software/expressionengine/ce-tweet/change-log.rss';