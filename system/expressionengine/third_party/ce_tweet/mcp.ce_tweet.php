<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CE Tweet - Module Control Panel File
 *
 * @author		Aaron Waldon
 * @copyright	Copyright (c) 2013 Causing Effect
 * @license		http://www.causingeffect.com/software/expressionengine/ce-cache/license-agreement
 * @link		http://www.causingeffect.com
 */

include( PATH_THIRD . 'ce_tweet/config.php' );

class Ce_tweet_mcp
{
	public function __construct()
	{
		$this->EE = get_instance();
	}
}
/* End of file */