<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

/*
====================================================================================================
 Author: Aaron Waldon (Causing Effect)
 http://www.causingeffect.com
 license http://www.causingeffect.com/software/expressionengine/ce-tweet/license-agreement
====================================================================================================
 This file must be placed in the /system/expressionengine/third_party/ce_tweet folder in your ExpressionEngine installation.
 package 		CE Tweet
 copyright 		Copyright (c) 2013 Causing Effect, Aaron Waldon <aaron@causingeffect.com>
----------------------------------------------------------------------------------------------------
 Purpose: Powerful image manipulation made easy
====================================================================================================

License:
    CE Tweet is software belonging to Aaron Waldon, all rights reserved.
	Here are a couple of specific points to keep in mind:
    * One license grants the right to perform one installation of CE Tweet. Each additional installation of CE Tweet requires an additional purchased license.
    * You may not reproduce, distribute, or transfer CE Tweet, or portions thereof, to any third party.
	* You may not sell, rent, lease, assign, or sublet CE Tweet or portions thereof.
	* You may not grant rights to any other person.
	* You may not use CE Tweet in violation of any United States or international law or regulation.
*/

include( PATH_THIRD . 'ce_tweet/config.php' );

class Ce_tweet_ft extends EE_Fieldtype {

	public $info = array(
		'name' => CE_TWEET_NAME,
		'version' => CE_TWEET_VERSION
	);

	private $default_settings = array( 'results_per_page' => 10 );

	private static $theme_folder_url;

	public function __construct()
	{
		parent::__construct();
		$this->EE->lang->loadfile( 'ce_tweet' );

		if ( empty( self::$theme_folder_url ) )
		{
			self::$theme_folder_url = defined('URL_THIRD_THEMES') ? URL_THIRD_THEMES : $this->EE->config->slash_item('theme_folder_url').'third_party/';
			self::$theme_folder_url .= 'ce_tweet/';
		}
	}

	public $has_array_data = true;

	/**
	 * Display field on the publish page
	 *
	 * @param array $data The existing data
	 * @param $is_matrix
	 * @return string field html
	 */
	public function display_field( $data, $is_matrix = false )
	{
		if ( ! isset( $is_matrix ) )
		{
			$is_matrix = false;
		}

		//load cp jquery files (needed for sortable)
		$this->EE->cp->add_js_script( array( 'ui' => array( 'core', 'position', 'widget', 'mouse', 'sortable', 'draggable', 'resizable' )));

		$ajax_search_url = $this->EE->functions->create_url( QUERY_MARKER . 'ACT=' . $this->EE->cp->fetch_action_id( CE_TWEET_CLASS, 'ajax_search' ) );
		$ajax_rate_limit_url = $this->EE->functions->create_url( QUERY_MARKER . 'ACT=' . $this->EE->cp->fetch_action_id( CE_TWEET_CLASS, 'ajax_rate_limit' ) );
		$ajax_show_url = $this->EE->functions->create_url( QUERY_MARKER . 'ACT=' . $this->EE->cp->fetch_action_id( CE_TWEET_CLASS, 'ajax_show' ) );

		//switch to https if the control panel is running it
		if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' )
		{
			$ajax_search_url = str_replace( 'http://', 'https://', $ajax_search_url );
			$ajax_rate_limit_url = str_replace( 'http://', 'https://', $ajax_rate_limit_url );
			$ajax_show_url = str_replace( 'http://', 'https://', $ajax_show_url );
		}

		//add the css and javascript
		$this->EE->cp->add_to_head( '
		<link rel="stylesheet" href="' . self::$theme_folder_url . '/css/ce_tweet.css">
		<script type="text/javascript">
			var CE_TWEET_SETTINGS = {
				url : {
					search : "' . $ajax_search_url . '",
					rate_limit : "' . $ajax_rate_limit_url . '",
					show : "' . $ajax_show_url . '"
				},
				lang : {
					ce_tweet_connection_problem : \'' . lang( 'ce_tweet_connection_problem' ) . '\',
					ce_tweet_moment_format : \'' . lang( 'ce_tweet_moment_format' ) . '\',
					ce_tweet_rate_message : \'' . lang( 'ce_tweet_rate_message' ) . '\',
					ce_tweet_rate_message_no_hits : \'' . lang( 'ce_tweet_rate_message_no_hits' ) . '\',
					ce_tweet_dialog_message : \'' . lang( 'ce_tweet_dialog_message' ) . '\',
					ce_tweet_dialog_title : \'' . lang( 'ce_tweet_dialog_title' ) . '\',
					ce_tweet_dialog_button_keep : \'' . lang( 'ce_tweet_dialog_button_keep' ) . '\',
					ce_tweet_dialog_button_remove : \'' . lang( 'ce_tweet_dialog_button_remove' ) . '\'
				},
				notices : {
					limit_low: false,
					limit_used: false,
					limit_initial: false
				}
			};
		</script>
		<script type="text/javascript" src="' . self::$theme_folder_url . 'js/ce_tweet.min.js"></script>' );

		//determine the field name
		$name = ( $is_matrix ) ? $this->cell_name : $this->field_name;

		//determine the results per page
		$results_per_page = ( isset( $this->settings['results_per_page'] ) ) ? $this->settings['results_per_page'] : 10;

		if ( ! $is_matrix ) //standard JavaScript
		{
			$this->EE->javascript->output('
				$(function(){
					new CE_TWEET( $( "#ce_tweet_container_' . $name . '" ), '.$results_per_page.' );
				});
			');
		}
		else //Matrix compatible JavaScript
		{
			$this->EE->javascript->output('
				Matrix.bind( "ce_tweet", "display", function(cell) {
					new CE_TWEET( $( cell.dom.$td ).find( ".ce_tweet_holder_outer"), '.$results_per_page.' );
				});
			');
		}

		//determine the saved tweet ids
		$tweet_ids = ( isset( $data ) ) ? $data : '';

		//construct the output
		$output = '<div class="ce_tweet_holder_outer"';
		if ( ! $is_matrix )
		{
			$output .= ' id="ce_tweet_container_' . $name . '"';
		}
		$output .= ' >
		<div class="ce_tweet_results_holder">
			<p><b>' . lang( 'ce_tweet_selected_heading' )  . '</b></p>
			<input class="ce_tweet_saved_hidden" type="hidden" name="' . $name . '" value="' . $tweet_ids . '" >
			<div class="ce_tweet_saved_results ce_tweet_sortable"><!-- --></div><!-- .ce_tweet_saved_results -->
		</div><!-- .ce_tweet_results_holder -->
		<div class="ce_tweet_search_holder">
			<p><b>' . lang( 'ce_tweet_search_heading' )  . '</b></p>
			<input class="ce_tweet_search" type="text" value="" />
			<div class="ce_tweet_search_results ce_tweet_sortable"><!-- --></div><!-- .ce_tweet_search_results -->
			<p class="ce_tweet_pagination"><a href="#" class="ce_tweet_load_more ce_tweet_hide">' . lang('ce_tweet_load_more') . '</a></p>
		</div><!-- .ce_tweet_search_holder -->' . PHP_EOL;
		$output .= '</div><!-- .ce_tweet_holder_outer -->';

		return $output;
	}

	/**
	 * Replace tag
	 *
	 * @param string $data
	 * @param array $params
	 * @param bool  $tagdata
	 * @return bool|string
	 */
	public function replace_tag( $data, $params = array(), $tagdata = false )
	{
		if ( trim($data) == '' || ! $tagdata ) //if there are no tweet ids or no tagdata, then we're done here
		{
			return $this->EE->TMPL->no_results();
		}

		//ids
		$ids = explode( '|', $data );

		//prefix
		$prefix = ! empty($params['prefix']) ? $params['prefix'] : '';

		//var prefix
		$var_prefix = '';
		if ( ! empty($prefix) )
		{
			$var_prefix = $prefix . ':';
		}

		//tag prefix
		$tag_prefix = '';
		if ( ! empty($prefix) )
		{
			$tag_prefix = ':' . $prefix;
		}

		//determine the type
		$type = (isset( $params['type'] ) && in_array($params['type'], array('loop', 'oembed', 'show'))) ? $params['type'] : 'loop';

		unset( $params['type'], $params['prefix'] );

		//the variables
		$variables = array();

		if ( $type == 'show' || $type == 'oembed' )
		{
			$output = $temp = '';

			//reconstruct the parameters
			$parameters = '';
			foreach ( $params as $param => $value )
			{
				$parameters .= ( strpos( $value, '"' ) === FALSE) ? "{$param}=\"{$value}\" " : "{$param}='{$value}' ";
			}

			$parameters = rtrim( $parameters );

			//loop through each parameter and output the final value
			foreach ( $ids as $index => $id )
			{
				$temp = '{exp:ce_tweet:'.$type.$tag_prefix.' id="'.$id.'" '.$parameters.'}';

				$temp .= str_replace(
					array( '{'.$var_prefix.'tweet_id'.'}', '{'.$var_prefix.'count'.'}' ),
					array( $id, $index + 1 ),
					$tagdata );

				$temp .= '{/exp:ce_tweet:'.$type.$tag_prefix.'}' . PHP_EOL;
				$output .= $temp;
			}

			return $output;
		}
		else //just a loop
		{
			foreach ( $ids as $index => $id )
			{
				$variables[] = array(
					$var_prefix.'tweet_id' => $id,
					$var_prefix.'count' => $index + 1
				);
			}

			return $this->EE->TMPL->parse_variables($tagdata, $variables);
		}
	}

/* ------------------------------------------------- Global Settings ------------------------------------------------- */
	/**
	 * Display global settings. This field is viewed in Add-Ons -> Fieldtypes -> CE Tweet (settings).
	 *
	 * @return string The form.
	 */
	public function display_global_settings()
	{
		//merge the settings and the post info
		$val = array_merge( $this->settings, $_POST );

		//validate the results per page
		$results_per_page = isset( $val['results_per_page'] ) ? $val['results_per_page'] : $this->default_settings['results_per_page'];
		$results_per_page = $this->validate_results_per_page( $results_per_page );

		//create the form
		$form = '';
		$form .= '<h3>Default Settings</h3>';
		$form .= form_label( lang( 'ce_tweet_results_per_page_label' ), 'ce_tweet_results_per_page' ).'<br>';
		$form .= form_input( 'results_per_page', $results_per_page, array( 'id' => 'ce_tweet_results_per_page' ) ) . '<br><br>';

		return $form;
	}

	/**
	 * The saves settings submitted from the display_global_settings() method.
	 *
	 * @return array The global settings
	 */
	public function save_global_settings()
	{
		if ( ! is_array( $this->settings ) )
		{
			$this->settings = $this->default_settings;
		}

		//merge the settings and the post info
		$val = array_merge( $this->settings, $_POST );

		//validate the results per page
		$results_per_page = isset( $val['results_per_page'] ) ? $val['results_per_page'] : $this->default_settings['results_per_page'];
		$results_per_page = $this->validate_results_per_page( $results_per_page );

		return array( 'results_per_page' => $results_per_page );
	}

/* ------------------------------------------------- Individual Settings ------------------------------------------------- */
	/**
	 * Display individual settings. This field is viewed in the Create a New Channel Field page.
	 *
	 * @param string $data
	 * @return string|void
	 */
	public function display_settings( $data )
	{
		$results_per_page = isset( $data['results_per_page'] ) ? $data['results_per_page'] : $this->settings['results_per_page'];

		$this->EE->table->add_row(
			lang( 'ce_tweet_results_per_page_label' ),
			form_input( 'results_per_page', $results_per_page )
		);
	}

	/**
	 * The saves settings submitted from the display_settings() method.
	 *
	 * @param string $data
	 * @return array|void The individual settings.
	 */
	public function save_settings( $data )
	{
		//validate the results per page
		$results_per_page = $this->EE->input->post( 'results_per_page' );
		$results_per_page = $this->validate_results_per_page( $results_per_page );

		return array(
			'results_per_page'	=> $results_per_page
		);
	}

	/**
	 * Validate the results per page.
	 *
	 * @param $results_per_page
	 * @return int A valid results per page.
	 */
	private function validate_results_per_page( $results_per_page )
	{
		//validate the results per page
		if ( ! is_numeric( $results_per_page ) )
		{
			$results_per_page = $this->default_settings['results_per_page'];
		}
		else if ($results_per_page < 1)
		{
			$results_per_page = 1;
		}
		else if ($results_per_page > 100)
		{
			$results_per_page = 100;
		}

		return $results_per_page;
	}


	/**
	 * Simple method to log an array of debug messages to the EE Debug console.
	 *
	 * @param array $messages The debug messages.
	 * @return void
	 */
	private function log_debug_messages( $messages = array() )
	{
		if ( is_string( $messages ) )
		{
			$messages = array( $messages );
		}

		foreach ( $messages as $message )
		{
			$this->EE->TMPL->log_item( '&nbsp;&nbsp;***&nbsp;&nbsp;CE Tweet fieldtype debug: ' . $message );
		}
	}


	/**
	 * Determines the given setting by checking for the param, and then for the global var, and then for the config item.
	 * @param string $name The name of the parameter. The string 'ce_tweet_' will automatically be prepended for the global and config setting checks.
	 * @param string $default The default setting value
	 * @return string The setting value if found, or the default setting if not found.
	 */
	private function determine_setting( $name, $default = '' )
	{
		$long_name = 'ce_tweet_' . $name;
		if ( isset( $this->EE->config->_global_vars[ $long_name ] ) && $this->EE->config->_global_vars[ $long_name ] !== FALSE ) //first check global array
		{
			$default = $this->EE->config->_global_vars[ $long_name ];
		}
		else if ( $this->EE->config->item( $long_name ) !== FALSE ) //then check config
		{
			$default = $this->EE->config->item( $long_name );
		}

		return $default;
	}

/* ------------------------------------------------- Miscellaneous ------------------------------------------------- */
	/**
	 * Install
	 *
	 * @access	public
	 * @return array The global settings.
	 */
	public function install()
	{
		return $this->default_settings;
	}

/* ------------------------------------------------- Matrix ------------------------------------------------- */
	/**
	 * Matrix display cell.
	 *
	 * @param $data
	 * @return string
	 */
	public function display_cell( $data )
	{
		return $this->display_field( $data, true );
	}

	/**
	 * Matrix display settings.
	 *
	 * @param $data
	 * @return array
	 */
	public function display_cell_settings( $data )
	{
		$results_per_page = isset( $data['results_per_page'] ) ? $data['results_per_page'] : $this->settings['results_per_page'];

		return array(
			array( lang( 'ce_tweet_results_per_page_label' ), form_input( 'results_per_page', $results_per_page ) )
		);
	}

	/**
	 * Matrix save settings.
	 *
	 * @param $data
	 * @return array
	 */
	public function save_cell_settings( $data )
	{
		//validate the results per page
		$results_per_page = isset( $data['results_per_page'] ) ? $data['results_per_page'] : $this->default_settings['results_per_page'];
		$data['results_per_page'] = $this->validate_results_per_page( $results_per_page );

		return $data;
	}
}

/* End of file ft.google_maps.php */
/* Location: ./system/expressionengine/third_party/google_maps/ft.google_maps.php */