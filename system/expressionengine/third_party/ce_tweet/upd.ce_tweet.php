<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CE Tweet - Module Update File
 *
 * @author		Aaron Waldon
 * @copyright	Copyright (c) 2013 Causing Effect
 * @license		http://www.causingeffect.com/software/expressionengine/ce-tweet/license-agreement
 * @link		http://www.causingeffect.com
 */

include( PATH_THIRD . 'ce_tweet/config.php' );

class Ce_tweet_upd {

	public $version = CE_TWEET_VERSION;

	private $EE;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->EE = get_instance();
	}

	/**
	 * Installs the module.
	 *
	 * @return bool
	 */
	public function install()
	{
		$this->uninstall();

		//register module
		$this->EE->db->insert('modules',
			array(
			'module_name' => CE_TWEET_CLASS,
			'module_version' => $this->version,
			'has_cp_backend' => 'n',
			'has_publish_fields' => 'n'
			)
		);

		$this->create_action( CE_TWEET_CLASS, 'ajax_search' );
		$this->create_action( CE_TWEET_CLASS, 'ajax_rate_limit' );
		$this->create_action( CE_TWEET_CLASS, 'ajax_show' );

		return true;
	}

	/**
	 * Uninstalls the module.
	 *
	 * @return bool
	 */
	function uninstall()
	{
		//remove the module
		$this->EE->db->where( 'module_name', CE_TWEET_CLASS )->delete( 'modules' );

		//remove the actions
		$this->EE->db->where( 'class', CE_TWEET_CLASS )->delete( 'actions' );

		return true;
	}

	/**
	 * Updates the module.
	 *
	 * @param string $current
	 * @return bool
	 */
	public function update( $current = '' )
	{
		return true;
	}

	/**
	 * Creates an action.
	 *
	 * @param string $class
	 * @param string $method
	 */
	protected function create_action( $class, $method )
	{
		$this->EE->db->where( 'class', $class )->where( 'method', $method );
		if ( $this->EE->db->count_all_results( 'actions' ) == 0 ) //if not already registered
		{
			//register action
			$this->EE->db->insert( 'actions', array( 'class' => $class, 'method' => $method ) );
		}
	}
}

/* End of file */